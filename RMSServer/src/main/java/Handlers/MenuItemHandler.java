/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import DataLayer.rmsdata.Tables;
import static DataLayer.rmsdata.Tables.CATEGORIES;
import static DataLayer.rmsdata.Tables.MENUITEMS_HAS_PRODUCTS;
import DataLayer.rmsdata.tables.Categories;
import static DataLayer.rmsdata.tables.Menuitems.MENUITEMS;
import DataLayer.rmsdata.tables.MenuitemsHasProducts;
import DataLayer.rmsdata.tables.Products;
import DataLayer.rmsdata.tables.records.CategoriesRecord;
import DataLayer.rmsdata.tables.records.MenuitemsHasProductsRecord;
import DataLayer.rmsdata.tables.records.MenuitemsRecord;
import DataLayer.rmsdata.tables.records.ProductsRecord;
import SQLConnector.SQLConnection;
import Tools.JSONTools;
import java.sql.SQLException;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.ParseException;

/**
 *
 * @author Bas
 */
public class MenuItemHandler extends SQLConnection {

    /**
     * Constructor that makes connection with database
     * @throws java.sql.SQLException
     */
    public MenuItemHandler() throws SQLException {
        super();
    }

    /**
     * Adds menuItem to database with multiple products
     *
     * @param JSONData string containing name, price, category, description  and products
     *
     */
    public void addMenuItem(String JSONData) throws ParseException {
        JSONObject data = JSONTools.JSONparser(JSONData);
        int menuItemID = (int) (long) data.get("id");
        String name = (String) data.get("name");
        String category = (String) data.get("category");
        String desc = (String) data.get("description");
        double price = (data.get("price") instanceof Long ? ((Long) data.get("price")).doubleValue() : (double) data.get("price"));
        JSONArray productIDs = (JSONArray) data.get("IDs");

        CategoriesRecord cat = DBContext.selectFrom(Categories.CATEGORIES).where(Categories.CATEGORIES.NAME.equal(category)).fetchOne();

        if (cat == null) {
            cat = DBContext.insertInto(Categories.CATEGORIES, Categories.CATEGORIES.NAME).values(category).returning(Categories.CATEGORIES.ID).fetchOne();
        }

        if (menuItemID == -1) {
            MenuitemsRecord newMenuItem = DBContext.insertInto(MENUITEMS, MENUITEMS.NAME, MENUITEMS.SALEPRICE, MENUITEMS.CATEGORIES_ID, MENUITEMS.DESCRIPTION).values(name, price, cat.getId(), desc).returning(MENUITEMS.ID).fetchOne();
            for (Object productID : productIDs) {
                Long id = (Long) productID;
                ProductsRecord prod = DBContext.selectFrom(Products.PRODUCTS).where(Products.PRODUCTS.ID.eq(Integer.parseInt(id.toString()))).fetchOne();
                if (prod != null) {
                    DBContext.insertInto(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS, MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID, MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID).values(newMenuItem.getId(), prod.getId()).execute();
                }
            }
        } else {
            // Update the MenuItem
            MenuitemsRecord updateMenuItem = DBContext.selectFrom(MENUITEMS).where(MENUITEMS.ID.eq(menuItemID)).fetchOne();
            updateMenuItem.setCategoriesId(cat.getId());
            updateMenuItem.setName(name);
            updateMenuItem.setDescription(desc);
            updateMenuItem.setSaleprice(price);
            updateMenuItem.update();

            // Get the MenuItemProducts for the updated Item
            Result<MenuitemsHasProductsRecord> menuItemProducts = DBContext.selectFrom(MENUITEMS_HAS_PRODUCTS).where(MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID.eq(menuItemID)).fetch();

            // Delete products from MenuItem
            for (MenuitemsHasProductsRecord menuItemProduct : menuItemProducts) {
                menuItemProduct.delete();
            }

            // Add products to MenuItem
            for (Object productID : productIDs) {
                Long id = (Long) productID;
                ProductsRecord prod = DBContext.selectFrom(Products.PRODUCTS).where(Products.PRODUCTS.ID.eq(Integer.parseInt(id.toString()))).fetchOne();
                if (prod != null) {
                    DBContext.insertInto(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS, MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID, MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID).values(updateMenuItem.getId(), prod.getId()).execute();
                }
            }
        }
    }

    /**
     * Delete menuItems from database (set desc to NA, because foreign key contraints it's dangerous to delete a whole menuItem)
     *
     * @param JSONData JSON string containing menuItemIDs
     *
     */
    public void deleteItems(String JSONData) throws ParseException {
        JSONObject data = JSONTools.JSONparser(JSONData);
        JSONArray menuItemIDs = (JSONArray) data.get("IDs");
        for (Object menuitemID : menuItemIDs) {
            Long id = (Long) menuitemID;
            MenuitemsRecord menuItem = DBContext.selectFrom(MENUITEMS).where(MENUITEMS.ID.equal(Integer.parseInt(id.toString()))).fetchOne();
            menuItem.setDescription("NA");
            menuItem.update();
        }
    }

    /**
     * Retrieves all menu items from database
     *
     * @return String all orders in JSON format
     */
    public String getAllMenuItems() {
        Result<?> results = DBContext.select(MENUITEMS.ID.as("id"), MENUITEMS.NAME.as("name"), MENUITEMS.SALEPRICE.as("saleprice"), MENUITEMS.CATEGORIES_ID.as("catid"), MENUITEMS.DESCRIPTION.as("desc"), Tables.STOCK.QUANTITY.min())
                .from(MENUITEMS.join(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS)
                        .on(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID.equal(MENUITEMS.ID)))
                .join(Tables.STOCK).on(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID.equal(Tables.STOCK.PRODUCT_ID))
                .where(MENUITEMS.DESCRIPTION.notEqual("NA"))
                .groupBy(MENUITEMS.ID).having(Tables.STOCK.QUANTITY
                .min()
                .greaterThan(0))
                .fetch();
        JSONArray resultset = new JSONArray();
        for (Record item : results) {
            JSONObject output = new JSONObject();
            output.put("id", item.get("id"));
            output.put("name", item.get("name"));
            output.put("saleprice", item.get("saleprice"));
            output.put("catid", item.get("catid"));
            output.put("desc", item.get("desc"));
            output.put("quantity", item.get("min"));
            resultset.add(output);
        }
        JSONObject result = new JSONObject();
        result.put("items", resultset);
        return result.toString();
    }

    /**
     * Retrieves menu item from database
     *
     * @param id id from the menu item
     * @return String one menu item in JSON format
     */
    public String getMenuItem(int id) {
        Record result = DBContext.select(MENUITEMS.ID.as("id"), MENUITEMS.NAME.as("name"), MENUITEMS.SALEPRICE.as("saleprice"), MENUITEMS.CATEGORIES_ID.as("catid"), MENUITEMS.DESCRIPTION.as("desc"), CATEGORIES.NAME.as("catname"))
                .from(MENUITEMS.join(CATEGORIES)
                        .on(CATEGORIES.ID.equal(MENUITEMS.CATEGORIES_ID)))
                .where(MENUITEMS.ID.equal(id)).fetchOne();
        Result<MenuitemsHasProductsRecord> productItems = DBContext.selectFrom(MENUITEMS_HAS_PRODUCTS).where(MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID.equal((int) result.get("id"))).fetch();

        JSONArray products = new JSONArray();
        for (Record product : productItems) {
            products.add(product.get("products_id"));
        }

        JSONObject output = new JSONObject();
        output.put("id", result.get("id"));
        output.put("name", result.get("name"));
        output.put("saleprice", result.get("saleprice"));
        output.put("catid", result.get("catid"));
        output.put("desc", result.get("desc"));
        output.put("catname", result.get("catname"));
        output.put("productIds", products);
        return output.toString();
    }

//    /**
//     * Edit a menuitem
//     *
//     * @param JSONData
//     * @throws ParseException
//     */
//    public void editMenuItem(String JSONData) throws ParseException {
//        JSONObject data = JSONTools.JSONparser(JSONData);
//        int menuItemID = (int) (long) data.get("id");
//        double saleprice = (double) data.get("saleprice");
//        String desc = (String) data.get("desc");
//        String name = (String) data.get("name");
//        int catID = (int) (long) data.get("catId");
//        MenuitemsRecord menuItem = DBContext.selectFrom(MENUITEMS).where(MENUITEMS.ID.equal(menuItemID)).fetchOne();
//        menuItem.setName(name);
//        menuItem.setDescription(desc);
//        menuItem.setSaleprice(saleprice);
//        menuItem.setCategoriesId(catID);
//        menuItem.update();
//    }
//    /**
//     * Edit the price of a menu item
//     *
//     * @param JSONInput
//     * @throws ParseException
//     */
//    public void EditMenuItem(String JSONInput) throws ParseException {
//        //Get properties from JSON input
//        JSONObject items = JSONTools.JSONparser(JSONInput);
//        int id = Integer.parseInt(items.get("id").toString());
//        int cat_id = Integer.parseInt(items.get("category").toString());
//        String desc = (String) items.get("description");
//        String name = (String) items.get("name");
//        double price = Double.parseDouble(items.get("saleprice").toString());
//        //Pull menuitem from database and edit it
//        MenuitemsRecord item = DBContext.selectFrom(MENUITEMS).where(MENUITEMS.ID.equal(id)).fetchOne();
//        item.setSaleprice(price);
//        item.setCategoriesId(cat_id);
//        item.setDescription(desc);
//        item.setName(name);
//        item.update();
//    }
}
