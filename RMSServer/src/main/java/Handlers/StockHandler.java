/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import DataLayer.rmsdata.Tables;
import DataLayer.rmsdata.tables.Menuitems;
import DataLayer.rmsdata.tables.MenuitemsHasProducts;
import DataLayer.rmsdata.tables.records.StockRecord;
import SQLConnector.SQLConnection;
import Tools.JSONTools;
import java.sql.SQLException;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.ParseException;

/**
 *
 * @author Bas
 */
public class StockHandler extends SQLConnection {

    /**
     * Constructor that makes connection with database
     */
    public StockHandler() throws SQLException {
        super();
    }

    /**
     * Update stock from product
     *
     * @param JSONData JSON string containing productID and amount of stock
     *
     */
    public void updateStockProduct(String JSONData) throws ParseException {
        JSONObject data = JSONTools.JSONparser(JSONData);
        int amount = (int) (long) data.get("amount");
        int productID = (int) (long) data.get("productID");
        StockRecord productStock = DBContext.selectFrom(Tables.STOCK).where(Tables.STOCK.PRODUCT_ID.eq(productID)).fetchOne();
        productStock.setQuantity(amount);
        productStock.update();
    }

    /**
     * Updates the stock from all products within menuItem with +1 when an item
     * is cancelled
     *
     * @param menuItemID Id of the cancelled menuItem
     *
     */
    public void replenishStockUponCancelling(int menuItemID) {
        Result<?> results = DBContext.select(Menuitems.MENUITEMS.ID.as("menuItemID"), MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID.as("productID")).from(Menuitems.MENUITEMS.join(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS).on(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.MENUITEMS_ID.equal(Menuitems.MENUITEMS.ID))).where(Menuitems.MENUITEMS.ID.eq(menuItemID)).orderBy(MenuitemsHasProducts.MENUITEMS_HAS_PRODUCTS.PRODUCTS_ID).fetch();
        for (Record product : results) {
            StockRecord prodStock = DBContext.selectFrom(Tables.STOCK).where(Tables.STOCK.PRODUCT_ID.equal(Integer.parseInt(product.get("productID").toString()))).fetchOne();
            prodStock.setQuantity(prodStock.getQuantity() + 1);
            prodStock.update();
        }
    }
}
