/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author basvg
 */
@Entity
@Table(name = "menuitems")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menuitems.findAll", query = "SELECT m FROM Menuitems m")
    , @NamedQuery(name = "Menuitems.findById", query = "SELECT m FROM Menuitems m WHERE m.id = :id")
    , @NamedQuery(name = "Menuitems.findByName", query = "SELECT m FROM Menuitems m WHERE m.name = :name")
    , @NamedQuery(name = "Menuitems.findBySaleprice", query = "SELECT m FROM Menuitems m WHERE m.saleprice = :saleprice")
    , @NamedQuery(name = "Menuitems.findByDescription", query = "SELECT m FROM Menuitems m WHERE m.description = :description")})
public class Menuitems implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saleprice")
    private float saleprice;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @JoinTable(name = "menuitems_has_products", joinColumns = {
        @JoinColumn(name = "menuitems_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "products_id", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Products> productsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menuitems")
    private Collection<OrdersHasMenuitems> ordersHasMenuitemsCollection;
    @JoinColumn(name = "categories_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Categories categoriesId;

    public Menuitems() {
    }

    public Menuitems(Integer id) {
        this.id = id;
    }

    public Menuitems(Integer id, String name, float saleprice) {
        this.id = id;
        this.name = name;
        this.saleprice = saleprice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(float saleprice) {
        this.saleprice = saleprice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Products> getProductsCollection() {
        return productsCollection;
    }

    public void setProductsCollection(Collection<Products> productsCollection) {
        this.productsCollection = productsCollection;
    }

    @XmlTransient
    public Collection<OrdersHasMenuitems> getOrdersHasMenuitemsCollection() {
        return ordersHasMenuitemsCollection;
    }

    public void setOrdersHasMenuitemsCollection(Collection<OrdersHasMenuitems> ordersHasMenuitemsCollection) {
        this.ordersHasMenuitemsCollection = ordersHasMenuitemsCollection;
    }

    public Categories getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(Categories categoriesId) {
        this.categoriesId = categoriesId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menuitems)) {
            return false;
        }
        Menuitems other = (Menuitems) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Listener.Menuitems[ id=" + id + " ]";
    }
    
}
