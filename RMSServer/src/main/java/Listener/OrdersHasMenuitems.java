/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basvg
 */
@Entity
@Table(name = "orders_has_menuitems")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrdersHasMenuitems.findAll", query = "SELECT o FROM OrdersHasMenuitems o")
    , @NamedQuery(name = "OrdersHasMenuitems.findByOrdersId", query = "SELECT o FROM OrdersHasMenuitems o WHERE o.ordersHasMenuitemsPK.ordersId = :ordersId")
    , @NamedQuery(name = "OrdersHasMenuitems.findByMenuitemsId", query = "SELECT o FROM OrdersHasMenuitems o WHERE o.ordersHasMenuitemsPK.menuitemsId = :menuitemsId")
    , @NamedQuery(name = "OrdersHasMenuitems.findByDelivered", query = "SELECT o FROM OrdersHasMenuitems o WHERE o.delivered = :delivered")})
public class OrdersHasMenuitems implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrdersHasMenuitemsPK ordersHasMenuitemsPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Delivered")
    private boolean delivered;
    @JoinColumn(name = "orders_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Orders orders;
    @JoinColumn(name = "menuitems_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Menuitems menuitems;

    public OrdersHasMenuitems() {
    }

    public OrdersHasMenuitems(OrdersHasMenuitemsPK ordersHasMenuitemsPK) {
        this.ordersHasMenuitemsPK = ordersHasMenuitemsPK;
    }

    public OrdersHasMenuitems(OrdersHasMenuitemsPK ordersHasMenuitemsPK, boolean delivered) {
        this.ordersHasMenuitemsPK = ordersHasMenuitemsPK;
        this.delivered = delivered;
    }

    public OrdersHasMenuitems(int ordersId, int menuitemsId) {
        this.ordersHasMenuitemsPK = new OrdersHasMenuitemsPK(ordersId, menuitemsId);
    }

    public OrdersHasMenuitemsPK getOrdersHasMenuitemsPK() {
        return ordersHasMenuitemsPK;
    }

    public void setOrdersHasMenuitemsPK(OrdersHasMenuitemsPK ordersHasMenuitemsPK) {
        this.ordersHasMenuitemsPK = ordersHasMenuitemsPK;
    }

    public boolean getDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Menuitems getMenuitems() {
        return menuitems;
    }

    public void setMenuitems(Menuitems menuitems) {
        this.menuitems = menuitems;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ordersHasMenuitemsPK != null ? ordersHasMenuitemsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdersHasMenuitems)) {
            return false;
        }
        OrdersHasMenuitems other = (OrdersHasMenuitems) object;
        if ((this.ordersHasMenuitemsPK == null && other.ordersHasMenuitemsPK != null) || (this.ordersHasMenuitemsPK != null && !this.ordersHasMenuitemsPK.equals(other.ordersHasMenuitemsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Listener.OrdersHasMenuitems[ ordersHasMenuitemsPK=" + ordersHasMenuitemsPK + " ]";
    }
    
}
