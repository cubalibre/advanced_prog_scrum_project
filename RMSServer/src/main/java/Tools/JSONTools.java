/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;
import org.jooq.tools.json.ParseException;

/**
 *
 * @author Margaux
 */
public  class JSONTools {

    /**
     * Parses input to JSON format
     *
     * @param input JSON data in string
     * @return JSONObject parsed from the inputstring
     */
    public static JSONObject JSONparser(String input) throws ParseException {
        JSONParser parser = new JSONParser();
        try {
            return (JSONObject) parser.parse(input);
        } catch (ClassCastException ex) {
            throw ex;
        }
    }
    
}
