var RMSAdmin = new function () {

    this.SelectedProduct = null;
    this.SelectedMenuItem = null;
    
    this.ToBeRemoved = {
        url: "null",
        id: -1,
        item: null
    }

    /*
     *  Shows a modal
     * @param {string} the selector of the modal (id)
     */
    this.showModal = function(selector) {
        $('.modal-cover').fadeIn(200);
        console.log($(selector));
        $(selector).removeClass('hidden');
    };

    /**
     * Toggles the admin tabs
     */
    this.changeTab = function (btn) {
        var tabId = btn.data('id');
        $('.admin-tab').removeClass('selected');
        btn.toggleClass('selected');

        // Color changes
        $('.btn-tab').removeClass('bg-green color-active-green').addClass('bg-grey color-active-grey');

        btn.find('.btn').addClass('bg-green color-active-green').removeClass('bg-grey color-active-grey');

        if (tabId === 'stockTab') {
            $('#stockContent').removeClass('hidden');
            $('#menuItemsContent').addClass('hidden');
        } else {
            $('#menuItemsContent').removeClass('hidden');
            $('#stockContent').addClass('hidden');
        }
    };


    /**
     * Loads the productdetails of one product from the database
     * @param {int} id The id of the product
     * @param {function} callback A callback function that will be called upon success
     */
    this.loadDetails = function(id, callback) {
        RMSApi.ajaxGet('api/products/' + id, callback);
        //RMSAdmin.ajax('api/products/' + id, 'json', 'get', (data, textStatus, jqXHR) => callback(data), null);
    };

    /**
     * Loads initial data and binds events
     */
    this.init = function () {
        $('.admin-tab').on('click', function () {
            RMSAdmin.changeTab($(this));
        });

        $('#btnNewMenuItem').on('click', function() {
            $('#modalMenuItem').attr('data-id', '-1').find('input').val('');
            RMSAdmin.setCheckboxes([]);
            RMSAdmin.showModal('#modalMenuItem');
        });
        
        $('#btnNewProduct').on('click', function() {
            $('#modalStock').attr('data-id', '-1').find('input').val('');
            RMSAdmin.showModal('#modalStock');
        });

        RMSAdmin.loadProductsStock(function (data) {
            var list = $('.stock-list');
            var restockList = $('#restockList');
            var menuProductsList = $('#menuProducts');
       
            $(data.products).each(function () {
                var htmlString1 = '<li data-id="' + this.id + '" class="clearfix margin-1-bottom"><label class="bg-grey">' + this.name + '</label><input class="number-field';
                var htmlString2 = '" type="number" min="0" max="9000" value="' + this.quantity + '"><button class="btn btn-sharp bg-yellow color-active-yellow btn-edit">&#9998;</button></li>';
                var htmlString3 = '" type="number" min="0" max="9000" value="' + this.quantity + '"></li>';
                if (this.quantity === 0) {
                    RMSAdmin.showModal('#modalRestock');

                    list.append(htmlString1 + ' bg-warning' + htmlString2);
                    restockList.append(htmlString1 + ' bg-warning' + htmlString3);
                } else {
                    list.append(htmlString1 + htmlString2);
                }
                
                menuProductsList.append('<li data-id="' + this.id + '" class="clearfix margin-1-bottom"><label class="bg-grey">' + this.name + '</label><label class="checkbox-wrapper"><p>&#10006;</p><input type="checkbox" ><div class="checkbox-replacement">&#10004;</div></label></li>')
            });
        });
        
        RMSAdmin.loadMenuItems(function(data) {
            var menuItems = $('#menuItems');

            console.log(data.items);

            $(data.items).each(function() {
                console.log(this);
                menuItems.append('<li data-id="'+ this.id +'" class="clearfix margin-1-bottom"><label class="bg-grey">'+ this.name +'</label><button class="btn btn-sharp bg-yellow color-active-yellow btn-edit-menuitem">&#9998;</button></li>');
            });
        });
    };

    /*
     * Loads all the menu items
     * @param {function} a callback that is called upon success
     */
    this.loadMenuItems = function(callback) {
        RMSApi.ajaxGet('api/menu/all', callback);
    };

    /**
     * Loads all the products and their stock from the database
     * @param {function} callback A callback that will be called upon success
     */
    this.loadProductsStock = function (callback) {
        RMSApi.ajaxGet('api/products/stock', callback);
    };

    /**
     * Edit a product in the database
     * @param {object} product The productinfo in the following style: {"productID":theId,"name":"theName","buyprice":theBuyPrice,"desc":"theDescription"}
     */
    this.editProduct = function(product) {
        RMSApi.ajaxPost('api/products/editProduct', product);
    };
    
    /*
     * Sets the checkboxes in the menuProductsView for a specific product.
     * @param {array} an array with all the product ids
     */
    this.setCheckboxes = function(ids) {
        $('#menuProducts').find('input').prop('checked', false);
        $(ids).each(function() {
            var id = parseInt(this);
            $('#menuProducts').children('li').each(function() {
                if($(this).data('id') === id) {
                    $(this).find('input').prop('checked', true);
                }
            });
        });
    };

    /**
     * Updates the stock of a product
     * @param {int} id The id of the product
     * @param {int} quantity The new quantity of the product
     */
    this.updateProductStock = function (id, quantity) {
        var product = {};
        product.productID = id;
        product.amount = parseInt(quantity);

        RMSApi.ajaxPost('api/products/update', product);
    };
};

$(window).on('load', function () {
    RMSAdmin.init();
});

// When you leave focus on an number-field (the stock input field) it will update the stock of that item
$(document).on('focusout', '.number-field', function () {
    RMSAdmin.updateProductStock($(this).parent().data('id'), $(this).val());
    $(this).val() === '0' ? $(this).addClass('bg-warning') : $(this).removeClass('bg-warning');
});

// When clicking on an edit button of a stock item, a modal will be shown with the info of the stockitem preloaded
$(document).on('click', '.btn-edit', function() {
    var id = $(this).parent().data('id');
    
    RMSAdmin.ToBeRemoved.url = 'api/products/delete';
    RMSAdmin.ToBeRemoved.id = id;
    RMSAdmin.ToBeRemoved.item = $(this).parent();
    RMSAdmin.SelectedProduct = $(this);

    RMSAdmin.loadDetails(id, function(data) {
        $('#productNameField').val(data.name);
        $('#productPriceField').val(data.buyprice);
        $('#productDescriptionField').val(data.desc);

        RMSAdmin.showModal('#modalStock');

        $('#modalStock').attr('data-id', data.id);
    });
});

$(document).on('click', '.btn-edit-menuitem', function() {
    $('#modalMenuItem').attr('data-id', $(this).parent().data('id'));
    RMSAdmin.SelectedMenuItem = $(this).parent();
    var id = $(this).parent().data('id');
    RMSAdmin.ToBeRemoved.url = 'api/menu/delete/menuItems';
    RMSAdmin.ToBeRemoved.id = id;
    RMSAdmin.ToBeRemoved.item = $(this).parent();
    RMSApi.ajaxGet('api/menu/' + id, function(data) {
        $('#menuNameField').val(data.name);
        $('#menuPriceField').val(data.saleprice);
        $('#menuDescriptionField').val(data.desc);
        $('#menuCategoryField').val(data.catname);
        RMSAdmin.showModal('#modalMenuItem');
        RMSAdmin.setCheckboxes(data.productIds);
    });
});

// when the submit button of the modal is clicked, the new info is submitted to the server, and the modal hides
$(document).on('click', '#buttonModalSubmit', function() {
    var product = {
        productID: parseInt($('#modalStock').attr('data-id')),
        name: $('#productNameField').val(),
        buyprice: parseFloat($('#productPriceField').val()),
        desc: $('#productDescriptionField').val()
    };
    
    if(product.productID !== -1) {
        RMSAdmin.SelectedProduct.siblings('label').text(product.name);
    }

    $('.modal-cover').fadeOut(200);
    $('#modalStock').addClass('hidden');

    RMSAdmin.editProduct(product);
});

$(document).on('click', '#buttonProductRemove', function() {
    RMSAdmin.showModal("#modalRemove");
    $('.modal-cover').css('zIndex', '4');
});

$(document).on('click', '#buttonMenuProductRemove', function() {
    RMSAdmin.showModal("#modalRemove");
    $('.modal-cover').css('zIndex', '4');
});

$(document).on('click', '#buttonComfirmRemove', function() {
    var data = {
        IDs: []
    }
    console.log(RMSAdmin.ToBeRemoved.id);
    data.IDs.push(RMSAdmin.ToBeRemoved.id);
    RMSApi.ajaxPost(RMSAdmin.ToBeRemoved.url, data);
    $('.modal-cover').fadeOut(200);
    $('.modal').addClass('hidden');
    $('.modal-cover').css('zIndex', '2');
    RMSAdmin.ToBeRemoved.item.slideUp();
});

$(document).on('click', '#buttonMenuItemsEdit', function() {
    var name = $(this).parent().find('#menuNameField').val();
    
    $('#menuProductsView').find('.subtitle').text('Products for ' + name);
    $('#AdminView').addClass('hidden');
    $('#menuProductsView').removeClass('hidden');
    $('.modal-cover').fadeOut(200);
    $(this).parent().addClass('hidden');
});

$(document).on('click', '#btnLinkProductsToMenu', function() {
    $('#menuProductsView').addClass('hidden');
    $('#AdminView').removeClass('hidden');
    RMSAdmin.showModal('#modalMenuItem');
});

$(document).on('click', '#buttonMenuItemSubmit', function() {
    var checkedItems = [];
    $('#menuProducts').find('input:checked').each(function() {
        checkedItems.push($(this).parents('li').data('id'));
    });
    
    var menuItem = {
        id: $('#modalMenuItem').data('id'),
        name: $('#menuNameField').val(),
        category: $('#menuCategoryField').val(),
        price: parseFloat($('#menuPriceField').val()),
        description: $('#menuDescriptionField').val(),
        IDs: checkedItems
    };
    console.log(JSON.stringify(menuItem));
    RMSApi.ajaxPost('api/menu/add/menuItem', menuItem);
    
    $('.modal-cover').fadeOut(200);
    $('.modal').addClass('hidden');
});

$(document).on('click', '#restockConfirm', function() {
    $('#modalRestock').addClass('hidden');
    $('.modal-cover').fadeOut(200);
    $('.modal-cover').css('zIndex', '2');
});


// when the cover is clicked, the modal hides
$(document).on('click', '.modal-cover', function() {
    $(this).fadeOut(200);
    $('.modal').addClass('hidden');
    $(this).css('zIndex', '2');
});