var RMSOrders = new function() {

    // holds the id of all the tables. This prevents the same table from appearing multiple times
    var tables = [];

    // Loads all the orders from the database using ajax
    this.loadOrders = function () {

        // clear tables
        tables = [];

        RMSApi.ajaxGet('api/orders/pending', RMSOrders.createOrderElements);
    };

    // Making elements in the Orders tab
    this.createOrderElements = function(data) {
        var orders = data['orders'];
        var orderList = $('#orderList');

        // Sorting data by importance descending (ID is like a timestamp)
        orders.sort((a,b) => a.orderId - b.orderId);

        orderList.empty();

        // Making elements in HTML
        orders.forEach((order) => {
            $('#orderList').append(
                '<li data-orderId="'+ order.orderId +'" data-itemId="'+ order.itemID +'" class="clearfix relative table-specific-item bg-grey padded margin-1-bottom">' +
                    '<p class="table-number-tag bg-yellow margin-1-bottom">' + order.tableId + '</p>' +
                    '<p class="order-specific-item-text">' + order.itemName + '</p>' +
                    '<button class="btn bg-green color-active-green btn-item-deliver">&#10003;</button>' +
                '</li>'
            );
        });

        // Buttons with check mark, to confirm complete order
        orderList.find('.btn').click(function(e) {
            var orderID = $(this).parent('li').data('orderid');
            var itemID = $(this).parent('li').data('itemid');

            data = {
                "orderID": orderID,
                "menuItemID": itemID
            };

            RMSApi.ajaxPost('api/orders/delivered', data);

            $(this).parent('li').slideUp();
        });
    }
};