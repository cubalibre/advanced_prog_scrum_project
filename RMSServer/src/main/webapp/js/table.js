var RMSTables = new function () {
    // A list containing the id's of the ordered items
    // (when adding an item to the order, it's id will be added to this array)
    this.orderedItems = [];

    // The selected table, -1 mean no selected table
    this.selectedTable = -1;

    // adds an order to the order list (adds it to the correct category)
    // also adds the item id to the orderedItems array
    this.addOrder = function () {
        var catId = $(this).parents('details').data('catid');
        var selectedItem = {
            name: $(this).text(),
            id: $(this).data('id')
        };

        var orderPanelListCategories = $('#orderPanelList').children('div');

        orderPanelListCategories.each(function () {
            if ($(this).data('catid') === catId) {
                $('#noOrderItems').addClass('hidden');
                $(this).removeClass('hidden');
                $(this).show(); // when a slideUp is called, it sets display to none. (without making use of the hidden class)
                $(this).append('<li class="clearfix"><label class="bg-grey">' + selectedItem.name + '</label><button data-id="' + selectedItem.id + '" class="btn btn-remove-order color-active-red right bg-red-lighter margin-1-bottom">✖</button></li>');
                RMSTables.orderedItems.push(selectedItem.id);
            }
        });
    };

    // Removes an order from the order list (also removes the id from the orderedItems array)
    this.removeOrder = function () {
        var selectedItem = {
            id: $(this).data('id')
        };

        if (RMSTables.orderedItems.indexOf(selectedItem.id) !== -1) {
            RMSTables.orderedItems.splice(RMSTables.orderedItems.indexOf(selectedItem.id), 1);
            $(this).parent().slideUp(200, function () {
                if ($(this).siblings().length === 1) {
                    $(this).parent().slideUp();
                }
                $(this).remove();
                if (RMSTables.orderedItems.length === 0) {
                    RMSApp.toggleOrderPanel();
                    $('#noOrderItems').removeClass('hidden');
                }
            });
        }
    };

    // Adds all the items to the item list (and their respective category)
    this.addItemsToItemList = function (items) {
        var categories = $('details');
        for (var i = 0; i < items['items'].length; i++) {
            var item = items['items'][i];
            categories.each(function (i) {
                if ($(categories[i]).data('catid') === item.catid) {
                    $(categories[i]).removeClass('hidden');
                    $(categories[i]).find('ul').append(
                            $('<li>').append(
                            $('<button>').attr('data-id', item.id).addClass('btn btn-item btn-menu-item bg-grey color-active-grey margin-1-bottom').text(item.name)
                            )
                            );
                }
            });
        }
    };

    // loads the categories from the database using an ajax call and our own API.
    // afterwards it will call the loadItems function
    this.loadCategories = function () {
        // callback function to load categories
        var onCategoriesSuccess = function (data) {
            var categories = data['categories'];

            for (var i = 0; i < categories.length; i++) {
                $('#productCategories').append('<details class="item-category bg-sidepanel hidden" data-catId="' + categories[i].id + '"><summary>' + categories[i].name + '</summary><ul class="item-list"></ul></details>');
                $('#orderPanelList').append('<div class="hidden" data-catId="' + categories[i].id + '"><p>' + categories[i].name + '</p></div>');
            }

            // Closes all the other category tabs when you open a new one. (max one open at a time)
            $('.item-category').on('click', function () {
                $(this).siblings('.item-category').each(function () {
                    this.open = false;
                });
            });

            // get menu items
            RMSApi.ajaxGet('api/menu/all', RMSTables.addItemsToItemList);
        };

        // get all categories
        RMSApi.ajaxGet('api/categories/all', onCategoriesSuccess);
    };

    this.clearOrder = function () {
        RMSTables.orderedItems = [];
        $('#orderPanelList').find('li').remove();
        $('.item-category').each(function () {
            this.open = false;
        });
    };

    this.loadTableStatus = function() {
        $('#tableContainer').children().each(function(e) {
            var tableId = $(this).data('table-id');

            RMSApi.ajaxGet('api/orders/' + tableId, function(data) {
                console.log(data);

                if (data.hasOwnProperty("paid") && data.paid === 1) {
                    $('#tableContainer').find('[data-table-id=' + tableId + ']').removeClass('occupied');
                } else {
                    $('#tableContainer').find('[data-table-id=' + tableId + ']').addClass('occupied');
                }
            });
        });
    };

    // Populate table view with all tables
    this.loadTables = function(RMSApp) {
        // callback function to load tables
        var onTablesSuccess = function (data) {

            var tables = data['tables'];
            $(tables).each(function (e) {
                $('#tableContainer').append('<button class="col-1 btn btn-table" data-table-id = ' + this.tableId + '>' + this.tableId + '</button>')
            });

            // Set view
            RMSApp.toggleView('.view-tables-all');

            // Change view on table click
            $('#tableContainer').children().on('click', function (e) {
                RMSApp.toggleView('.view-table-details');

                RMSTables.selectedTable = $(this).attr('data-table-id');
                var data = null;

                // callback function to load orders by table
                var onTableOrderSuccess = function (data) {
                    var pendingProducts = $('#pendingProducts').find('.item-list');
                    var deliveredProducts = $('#deliveredProducts').find('.item-list');
                    pendingProducts.html('');
                    deliveredProducts.html('');

                    if (data['paid'] === 1) {
                        return false;
                    }

                    var orderId = data['orderId'];

                    $(data['menuItems']).each(function () {
                        if (this.delivered) {
                            deliveredProducts.append('<li class="clearfix margin-1-bottom"><label class="bg-grey">' + this.menuItemName + '</label></li>');
                        } else {
                            pendingProducts.append('<li class="clearfix"><label class="bg-grey">' + this.menuItemName + '</label><button data-menuId="' + this.menuId + '" data-orderid="' + orderId + '" class="btn btn-cancel-order color-active-red right bg-red-lighter margin-1-bottom">✖</button></li>')
                        }
                    });
                };

                // get orders by table
                RMSApi.ajaxGet('api/orders/' + RMSTables.selectedTable, onTableOrderSuccess);

                $('.header-title').html('Table ' + RMSTables.selectedTable);
                $('.view-table-details').find('h2').text('Table actions');

                // Push view into history
                RMSApp.history.push({
                    title: 'Table ' + RMSTables.selectedTable,
                    view: '.view-table-details',
                    action: null
                });
            });
            RMSTables.loadTableStatus();
        };

        // get all tables
        RMSApi.ajaxGet('api/tables/all', onTablesSuccess);
    };

    this.init = function () {

        RMSTables.loadCategories(RMSApp);

        RMSTables.loadTables(RMSApp);

        $(document).on('click', '.btn-menu-item', this.addOrder);
        
        $(document).on('click', '.btn-remove-order', this.removeOrder);

        // Open modal to confirm the total price and allow checkout
        $(document).on('click', '#btnCheckout', function() {
            var onOrderPriceSucces = function (data) {
                if (data.totalPrice == 0) {
                    $('#checkoutModalLabel').text('No items selected!');
                    $('#checkoutModalTitle').text('Checkout for table ' + RMSTables.selectedTable);
                    $('.modal-cover').fadeIn(200);
                    $('#checkoutModal').removeClass('hidden');
                    $('#buttonCheckoutModalSubmit').attr('disabled', true);
                }
                else {
                    $('#checkoutModalLabel').text('Checkout price: ' + data.totalPrice);
                    $('#checkoutModalTitle').text('Checkout for table ' + RMSTables.selectedTable);
                    $('.modal-cover').fadeIn(200);
                    $('#checkoutModal').removeClass('hidden');
                    $('#buttonCheckoutModalSubmit').attr('disabled', false);
                }
            };

            RMSApi.ajaxGet('api/orders/totalOrderPrice/' + RMSTables.selectedTable, onOrderPriceSucces);
        });

        // Check out the order
        $(document).on('click', '#buttonCheckoutModalSubmit', function () {
            RMSApi.ajaxGet('api/tables/checkout/' + RMSTables.selectedTable, function () {

            });
            $('#tableContainer').find('[data-table-id=' + RMSTables.selectedTable + ']').removeClass('occupied');

            $('.modal-cover').fadeOut(200);
            $('#checkoutModal').addClass('hidden');
            RMSApp.toggleView('.view-tables-all');
        });

        // when the modal cover is clicked, the modal hides
        $(document).on('click', '.modal-cover', function() {
            $(this).fadeOut(200);
            $('.modal').addClass('hidden');
        });

        // Allows you to add products to the order
        $(document).on('click', '#btnAddProducts', function () {
            RMSApp.toggleView('.view-order-for-table');

            RMSApp.history.push({
                title: 'Table ' + RMSTables.selectedTable,
                view: '.view-order-for-table',
                action: null
            });
        });

        $(document).on('click', '.btn-cancel-order', function () {
            var itemData = {
                menuItemId: $(this).data('menuid'),
                orderId: $(this).data('orderid')
            };

            RMSApi.ajaxPost('api/orders/cancel/menuItem', itemData);

            $(this).parent().remove();
        });

        // places order using ajax when pressing '.btn-place-order'
        $('.btn-place-order').on('click', function () {

            if (RMSTables.selectedTable === -1) {
                return false;
            }

            $('#tableContainer').find('[data-table-id=' + RMSTables.selectedTable + ']').addClass('occupied');
            
            var order = {
                // tableId and employeeId are currently fixed, hardcoded numbers. this will be made dynamic eventually.
                tableid: RMSTables.selectedTable,
                employeeid: 34,
                menuitems: RMSTables.orderedItems
            };
            console.log(JSON.stringify(order));
            RMSApi.ajaxPost('api/orders/add', order);
            //RMSApp.ajax('api/orders/add', 'text', 'post', null, JSON.stringify(order));

            // Close panel when order placed
            RMSApp.toggleOrderPanel();
            RMSApp.toggleView('.view-tables-all');

            RMSApp.history = [{
                title: 'Tables',
                view: '.view-tables-all',
                action: null
            }];
        });
    };
};