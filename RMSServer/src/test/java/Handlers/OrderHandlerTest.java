/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import static DataLayer.rmsdata.Tables.ORDERS;
import DataLayer.rmsdata.tables.OrdersHasMenuitems;
import DataLayer.rmsdata.tables.records.OrdersHasMenuitemsRecord;
import DataLayer.rmsdata.tables.records.OrdersRecord;
import Tools.JSONTools;
import static Tools.JSONTools.JSONparser;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import java.sql.SQLException;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TOM
 */
public class OrderHandlerTest {

    public OrderHandlerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    /**
//     * Test of checkoutOrder method, of class OrderHandler.
//     */
//    @Test
//    public void testCheckoutOrder() {
//        System.out.println("checkoutOrder");
//        int tableId = 0;
//        OrderHandler instance = new OrderHandler();
//        instance.checkoutOrder(tableId);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of createOrder method, of class OrderHandler.
     */
    @Test
    public void testCreateOrder() throws Exception {
        System.out.println("createOrder");
        OrderHandler instance = new OrderHandler();
        MenuItemHandler menuHandler = new MenuItemHandler();

        int tableID = 74;
        int employeeID = 34;
        int[] menuItemsArray = {95};
        double price = getTotalPriceMenuItems(menuHandler, menuItemsArray);

        // Get open order
        JSONObject oldOrder = JSONparser(instance.getOpenOrdersForTable(tableID));
        byte paid = (byte) (long) oldOrder.get("paid");

        // Create new order
        if (paid == 1) {

            // Create order
            createOrder(instance, menuItemsArray, tableID, employeeID);

            // Get new order
            JSONObject newOrder = JSONparser(instance.getOpenOrdersForTable(tableID));

            // Check if order is added
            assertEquals((byte) (long) newOrder.get("paid"), 0);

            // Check if price is correct
            JSONObject totalPriceJSON = JSONparser(instance.getTotalOrderPrice(tableID));
            double totalPrice = (double) totalPriceJSON.get("totalPrice");

            assertEquals(price, totalPrice, 0.0);

            // Get orderID
            int newOrderID = (int) (long) newOrder.get("orderId");

            // Get all orders
            String all = instance.getOrdersAll();
            JSONObject allOrders = JSONparser(all);
            JSONArray allOrdersArray = (JSONArray) allOrders.get("orders");

            JSONObject last = (JSONObject) allOrdersArray.get(allOrdersArray.size() - 1);

            assertEquals(last.get("orderId"), newOrder.get("orderId"));

            // Delete menuItems from order
            for (int i = 0; i < menuItemsArray.length; i++) {
                OrdersHasMenuitemsRecord deleteMenuitem = instance.DBContext.selectFrom(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS)
                        .where(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.ORDERS_ID.equal(newOrderID))
                        .and(OrdersHasMenuitems.ORDERS_HAS_MENUITEMS.MENUITEMS_ID.equal(menuItemsArray[i]))
                        .limit(1).fetchOne();
                byte delivered = deleteMenuitem.getDelivered();
                assertEquals(delivered,(byte) 1);
                deleteMenuitem.delete();
            }

            // checkoutOrder
            instance.checkoutOrder(tableID);

            // Delete the order
            OrdersRecord deleteOrder = instance.DBContext.selectFrom(ORDERS).where(ORDERS.ID.equal(newOrderID)).fetchOne();
            deleteOrder.delete();

        } // Add to existing order
        else if (paid == 0) {
            // Get orderID
            int oldOrderID = (int) (long) oldOrder.get("orderId");

            // Add to existing order
            createOrder(instance, menuItemsArray, tableID, employeeID);

            // Get new order
            JSONObject newOrder = JSONparser(instance.getOpenOrdersForTable(tableID));
            int newOrderID = (int) (long) oldOrder.get("orderId");

            // checkoutOrder
            instance.checkoutOrder(tableID);

            assertEquals(oldOrderID, newOrderID);

        }
    }

//    /**
//     * Test of getOpenOrdersForTable method, of class OrderHandler.
//     */
//    @Test
//    public void testGetOpenOrdersForTable() {
//        System.out.println("getOpenOrdersForTable");
//        int id = 0;
//        OrderHandler instance = new OrderHandler();
//        String expResult = "";
//        String result = instance.getOpenOrdersForTable(id);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getOrderWithID method, of class OrderHandler.
     */
    @Test
    public void testGetOrderWithID() throws SQLException {
        System.out.println("getOrderWithID");
        int orderID = 287;
        OrderHandler instance = new OrderHandler();

        instance.makeConnection();
        assertThat(instance.getOrderWithID(orderID), matchesJsonSchemaInClasspath("Order.json"));
    }
//
//    /**
//     * Test of getOrdersAll method, of class OrderHandler.
//     */
//    @Test
//    public void testGetOrdersAll() {
//        System.out.println("getOrdersAll");
//        OrderHandler instance = new OrderHandler();
//        String expResult = "";
//        String result = instance.getOrdersAll();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of removeMenuItemFromOrder method, of class OrderHandler.
//     */
//    @Test
//    public void testRemoveMenuItemFromOrder() throws Exception {
//        System.out.println("removeMenuItemFromOrder");
//        String JSONData = "";
//        OrderHandler instance = new OrderHandler();
//        instance.removeMenuItemFromOrder(JSONData);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//

    /**
     * Test of getAllPendingItems method, of class OrderHandler.
     */
    @Test
    public void testGetAllPendingItems() throws Exception {
        System.out.println("getAllPendingItems");
        OrderHandler instance = new OrderHandler();

        instance.makeConnection();
        assertThat(instance.getAllPendingItems(), matchesJsonSchemaInClasspath("Orders_Pending.json"));
    }
//
//    /**
//     * Test of getTotalOrderPrice method, of class OrderHandler.
//     */
//    @Test
//    public void testGetTotalOrderPrice() throws Exception {
//        System.out.println("getTotalOrderPrice");
//        int tableId = 0;
//        OrderHandler instance = new OrderHandler();
//        String expResult = "";
//        String result = instance.getTotalOrderPrice(tableId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of deliverMenuItem method, of class OrderHandler.
//     */
//    @Test
//    public void testDeliverMenuItem() throws Exception {
//        System.out.println("deliverMenuItem");
//        String dataJSON = "";
//        OrderHandler instance = new OrderHandler();
//        instance.deliverMenuItem(dataJSON);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Creates a new order
     *
     * @param instance
     * @param menuItemsArray
     * @param tableID
     * @param employeeID
     * @throws ParseException
     */
    private void createOrder(OrderHandler instance, int[] menuItemsArray, int tableID, int employeeID) throws ParseException {
        // Create new orderJSONObject with menuItems
        JSONArray menuItems = new JSONArray();
        for (int i = 0; i < menuItemsArray.length; i++) {
            menuItems.add(menuItemsArray[i]);
        }

        JSONObject newOrderJSON = new JSONObject();
        newOrderJSON.put("tableid", tableID);
        newOrderJSON.put("employeeid", employeeID);
        newOrderJSON.put("menuitems", menuItems);

    
        
        // Create new order
        instance.createOrder(newOrderJSON.toString());
        
        //deliver one of the items
        JSONObject itemToBeDelivered = new JSONObject();
        itemToBeDelivered.put("menuItemID", menuItemsArray[0]);
        JSONObject lastOrder = JSONparser(instance.getOpenOrdersForTable(tableID));
        int lastOrderID = (int) (long)lastOrder.get("orderId");
        itemToBeDelivered.put("orderID", lastOrderID);
        instance.deliverMenuItem(itemToBeDelivered.toString());
    }

    /**
     * Gets price of the menuitems
     *
     * @param handler
     * @param menuItemsArray
     * @return totalPrice of all menuItems
     * @throws ParseException
     */
    private double getTotalPriceMenuItems(MenuItemHandler handler, int[] menuItemsArray) throws ParseException {
        double price = 0;
        for (int i = 0; i < menuItemsArray.length; i++) {
            JSONObject menuItemJSON = JSONparser(handler.getMenuItem(menuItemsArray[i]));
            price += (double) menuItemJSON.get("saleprice");
        }

        return price;
    }

}
