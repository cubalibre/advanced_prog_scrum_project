/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Handlers;

import static DataLayer.rmsdata.Tables.PRODUCTS;
import static DataLayer.rmsdata.Tables.STOCK;
import DataLayer.rmsdata.tables.records.ProductsRecord;
import DataLayer.rmsdata.tables.records.StockRecord;
import static Tools.JSONTools.JSONparser;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import java.sql.SQLException;
import org.jooq.Result;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author basvg
 */
public class ProductHandlerTest {

    public ProductHandlerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {

    }

    /**
     * Test of getProduct method, of class ProductHandler.
     */
    @Test
    public void testGetProduct() throws ParseException, SQLException {
        System.out.println("getProduct");
        ProductHandler instance = new ProductHandler();
        instance.makeConnection();
        assertThat(instance.getProduct(14), matchesJsonSchemaInClasspath("Product.json"));

    }

    /**
     * Test of getAllProducts method, of class ProductHandler.
     */
    @Test
    public void testGetAllProducts() throws SQLException {
        System.out.println("getAllProducts");
        ProductHandler instance = new ProductHandler();
        instance.makeConnection();
        assertThat(instance.getAllProducts(), matchesJsonSchemaInClasspath("All_Products.json"));
        instance.Close();
    }

    /**
     * Test of updateProduct method, of class ProductHandler.
     */
    @Test
    public void testUpdateProduct() throws Exception {
        System.out.println("updateProduct");
        ProductHandler instance = new ProductHandler();
        instance.makeConnection();
        //create product to be updated
        JSONObject product = new JSONObject();
        product.put("productID", 14);
        product.put("buyprice", 0.95);
        product.put("desc", "Unit test is working");
        product.put("name", "Fristi");

        //update the product
        instance.updateProduct(product.toString());

        JSONObject newproduct = JSONparser(instance.getProduct(14));
        newproduct.put("productID", newproduct.get("id"));
        newproduct.remove("id");
        //check if new product matches 
        assertEquals(newproduct.toString(), product.toString());

        //revert product
        product.put("productID", 14);
        product.put("buyprice", 0.90);
        product.put("desc", "Heerlijke fristi mmmm");
        product.put("name", "Fristi");

        instance.updateProduct(product.toString());

        instance.Close();
    }

    /**
     * Test of deleteProducts method, of class ProductHandler.
     */
    @Test
    public void testDeleteProducts() throws Exception {
        System.out.println("deleteProducts");
        String JSONData = "{\"productID\":-1,\"name\":\"Vurte Sla\",\"buyprice\":0.01,\"desc\":\"Bruine vorte kropsla met schimmel\"}";
        ProductHandler instance = new ProductHandler();
        instance.updateProduct(JSONData);

        JSONObject allProducts = JSONparser(instance.getAllProducts());
        JSONArray products = (JSONArray) allProducts.get("products");
        JSONObject lastProduct = (JSONObject) products.get(products.size() - 1);
        instance.deleteProducts("{\"IDs\": [" + lastProduct.get("id") + "]}");
        int id = (int) (long) lastProduct.get("id");
        allProducts = JSONparser(instance.getAllProducts());
        products = (JSONArray) allProducts.get("products");
        JSONObject newProduct = (JSONObject) products.get(products.size() - 1);

        assertNotEquals(lastProduct, newProduct);
        
        StockRecord deleteMeToo = instance.DBContext.selectFrom(STOCK).where(STOCK.PRODUCT_ID.equal(id)).fetchOne();
        deleteMeToo.delete();
        
        ProductsRecord deleteMe = instance.DBContext.selectFrom(PRODUCTS).where(PRODUCTS.ID.equal(id)).fetchOne();
        deleteMe.delete();
        

        

    }
}
