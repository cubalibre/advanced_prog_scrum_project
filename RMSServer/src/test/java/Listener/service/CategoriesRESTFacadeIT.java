/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.filter.log.ResponseLoggingFilter;
import com.jayway.restassured.http.ContentType;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import com.jayway.restassured.specification.RequestSpecification;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TOM
 */
public class CategoriesRESTFacadeIT {

    private RestValidator validator;

    public CategoriesRESTFacadeIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        validator = new RestValidator("categories");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCategory method, of class CategoriesRESTFacade.
     */
    @Test
    public void testGetCategory() {
        System.out.println("getCategory");

        int categoryID = 4;
        
        validator.getRequest("" + categoryID, "Category.json");
    }

    /**
     * Test of getAllCategories method, of class CategoriesRESTFacade.
     */
    @Test
    public void testGetAllCategories() {
        System.out.println("getAllCategories");

        validator.getRequest("all", "All_Categories.json");
    }
}
