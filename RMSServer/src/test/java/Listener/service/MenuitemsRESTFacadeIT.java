/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import com.jayway.restassured.response.Response;
import org.jooq.tools.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TOM
 */
public class MenuitemsRESTFacadeIT {

    private RestValidator validator;

    public MenuitemsRESTFacadeIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        validator = new RestValidator("menu");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getMenuItem method, of class MenuitemsRESTFacade.
     */
    @Test
    public void testGetMenuItem() {
        System.out.println("getMenuItem");

        int menuItemID = 24;

        validator.getRequest("" + menuItemID, "MenuItem.json");
    }

    /**
     * Test of getAllMenuItems method, of class MenuitemsRESTFacade.
     */
    @Test
    public void testGetAllMenuItems() {
        System.out.println("getAllMenuItems");

        validator.getRequest("all", "All_MenuItems.json");
    }

//    /**
//     * Test of editPrice method, of class MenuitemsRESTFacade.
//     */
//    @Test
//    public void testEditPrice() {
//        System.out.println("editPrice");
//
//        JSONObject menuItem = new JSONObject();
//        menuItem.put("id", 74);
//        menuItem.put("category", 94);
//        menuItem.put("description", "Integration test is working");
//        menuItem.put("name", "Chocomousse");
//        menuItem.put("saleprice", 2.85);
//
//        Response res = validator.postRequest("edit", menuItem);
//
//        assertTrue(res.asString().contains("edited"));
//    }

//    /**
//     * Test of addMenuItem method, of class MenuitemsRESTFacade.
//     */
//    @Test
//    public void testAddMenuItem() {
//        System.out.println("addMenuItem");
//
//        JSONArray IDsArray = new JSONArray();
//        IDsArray.add(116);
//        IDsArray.add(117);
//
//        JSONObject menuItem = new JSONObject();
//        menuItem.put("name", "MenuItemIntegration");
//        menuItem.put("category", "Test Category");
//        menuItem.put("description", "This is a menuItem to test integrationtests");
//        menuItem.put("price", 1.20);
//        menuItem.put("IDs", IDsArray);
//
//        Response res = validator.postRequest("add/menuItem", menuItem);
//
//        assertTrue(res.asString().contains("created"));
//    }

//    /**
//     * Test of deleteMenuItems method, of class MenuitemsRESTFacade.
//     */
//    @Test
//    public void testDeleteMenuItems() {
//        System.out.println("deleteMenuItems");
//
//        JSONArray IDsArray = new JSONArray();
//        IDsArray.add(97);
//        IDsArray.add(98);
//
//        JSONObject IDs = new JSONObject();
//        IDs.put("IDs", IDsArray);
//
//        Response res = validator.postRequest("delete/menuItems", IDs);
//
//        assertTrue(res.asString().contains("deleted"));
//    }
}
