/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listener.service;

import static com.jayway.restassured.RestAssured.given;
import com.jayway.restassured.builder.RequestSpecBuilder;
import static com.jayway.restassured.config.EncoderConfig.encoderConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.filter.log.ResponseLoggingFilter;
import com.jayway.restassured.http.ContentType;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.jooq.tools.json.JSONObject;

/**
 *
 * @author TOM
 */
public class RestValidator {

    private final RequestSpecification RequestSpecification;

    public RequestSpecification getRequestSpecification() {
        return RequestSpecification;
    }

    public RestValidator(String restFacadeURI) {
        this.RequestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.ANY)
                .setBaseUri("http://localhost:8080/RMSServer/api/" + restFacadeURI)
                .addFilter(new ResponseLoggingFilter())
                .build();
    }

    /**
     * Performs a post request
     *
     * @param path URI path for post
     * @param rawData The JSON data to send in the body
     * @return Response if post was successful
     */
    public Response postRequest(String path, JSONObject rawData) {

        return given()
                .spec(RequestSpecification)
                .config(RestAssuredConfig.config().encoderConfig(encoderConfig().encodeContentTypeAs("*/*", ContentType.TEXT)))
                .body(rawData.toString())
                .post(path);
    }

    /**
     * Performs a get request
     *
     * @param path URI path for get
     * @param JSONSchema Name of the JSONSchema
     */
    public void getRequest(String path, String JSONSchema) {

        given()
                .spec(RequestSpecification)
                .get(path)
                .then()
                .assertThat()
                .body(matchesJsonSchemaInClasspath(JSONSchema));
    }
}
